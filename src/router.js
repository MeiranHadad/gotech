const express = require('express');
const QuestionnaireController = require('./questionnaire/questionnaire.controller');

const router = express.Router();

router.post('/questionnaire', QuestionnaireController.createQuestionnaire);
router.post('/qa', QuestionnaireController.createQa);

router.get('/:id', QuestionnaireController.getById);
router.get('/', QuestionnaireController.getMany);
router.put('/answers', QuestionnaireController.saveAnswers);

module.exports = router;