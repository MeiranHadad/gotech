require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const routes = require('./router');
const bodyParser = require("body-parser");


const setHeaders = (
    req, res, next
  ) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Credentials', 'true');
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    res.setHeader(
      'Access-Control-Allow-Headers',
      'Authorization, Origin, X-Requested-With, Content-Type',
    );
    next();
};

const mongoString = process.env.DATABASE_URL;

mongoose.connect(mongoString);
const database = mongoose.connection;

database.on('error', (error) => {
    console.log(error)
})

database.once('connected', () => {
    console.log('Database Connected');
})

const app = express();

app.use(setHeaders);

app.use(bodyParser.urlencoded({
  extended: true
}));

app.use(bodyParser.json())

app.use('/api', routes);


app.listen(3000, () => {
    console.log(`Server Started at ${3000}`)
})