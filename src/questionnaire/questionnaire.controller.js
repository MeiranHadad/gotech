const QuestionnaireManager = require('./questionnaire.manager');

module.exports = class QuestionnaireController {

    static async createQuestionnaire(req, res) {
        res.send(await QuestionnaireManager.createQuestionnaire(req.body));
    }

    static async createQa(req, res) {
        res.send(await QuestionnaireManager.createQa(req.body));
    }
    
    static async getById(req, res) {
        res.send(await QuestionnaireManager.getById(req.params.id));
    }
    
    static async getMany(req, res) {
        res.send(await QuestionnaireManager.getMany(req.query || ''));
    }

    static async saveAnswers(req, res) {
        res.send(await QuestionnaireManager.saveAnswers(req.body));
    }
};