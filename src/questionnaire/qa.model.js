const mongoose = require('mongoose');

const qaSchema = new mongoose.Schema({
    question: {
        required: true,
        type: String
    },
    answer: [{
        type: String,
        default: ''
    }],
    isRequired: {
        required: true,
        type: Boolean
    },
    questionnaireId: {
        require: true,
        type: String
    },
})

module.exports = mongoose.model('Qa', qaSchema)