const mongoose = require('mongoose');

const questionnaireSchema = new mongoose.Schema({
    questionnaireName: {
        required: true,
        type: String
    },
    qaIds: {
        type: [String],
        default: [],
    },
    date: {
        type: Date,
        default: new Date()
    }
})

module.exports = mongoose.model('Questionnaire', questionnaireSchema)