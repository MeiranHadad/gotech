const QuestionnaireRepository = require('./questionnaire.repository');

module.exports = class QuestionnaireManager {

    static createQuestionnaire(questionnaire) {
        return QuestionnaireRepository.createQuestionnaire(questionnaire);
    }

    static async createQa(qa) {
        const qaCreated = await QuestionnaireRepository.createQa(qa);
        return QuestionnaireRepository.updateQuestionnaire(qaCreated.questionnaireId ,qaCreated.id);
    }
    
    static getById(id) {
        return QuestionnaireRepository.getById(id);
    }
    
    static getMany() {
        return QuestionnaireRepository.getMany();
    }

    static saveAnswers(answers) {
        return QuestionnaireRepository.saveAnswers(answers);
    }
};