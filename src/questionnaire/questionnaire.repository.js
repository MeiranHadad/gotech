const QuestionnaireModel = require('./questionnaire.model');
const QaModel = require('./qa.model');

const mongoose = require('mongoose');

module.exports = class QuestionnaireRepository {

    static createQuestionnaire(questionnaire) {
        return QuestionnaireModel.create(questionnaire);
    }

    static createQa(qa) {
        return QaModel.create(qa);
    }
    
    static getById(id) {
        return QaModel.find({questionnaireId: { $in: id}});
    }
    
    static getMany() {
        return QuestionnaireModel.find().exec();
    }

    static updateQuestionnaire(questionnaireId, qaId) {
        return QuestionnaireModel.findByIdAndUpdate(
            questionnaireId,
            { $push: { qaIds: qaId} },
        )
        .exec();
    }

    static saveAnswers(questionIdAnswerPair) {

        return QaModel.aggregate([
            { $match: { _id: mongoose.Types.ObjectId(questionIdAnswerPair.questionId) } },
            { $set: { answer: questionIdAnswerPair.answer}},
        ]);
    }
    
};